# MacBook Battery Percentage Corrector

A simple GNOME shell extension to correct battery percentage in the top-right corner.

This extension does _not_ mess with power subsystems at all; it simply reads the appropriate files from `/sys/class/power_supply/BAT0` to determine battery percentage while factoring in battery degredation.

Read a little more about this at the [Mysore LUG Website](https://mysorelug.indriyallc.net/articles/2021/06/writing-gnome-extension/index.html).
The extension itself is available at the [GNOME shell extensions website](https://extensions.gnome.org/extension/4353/macbook-battery-percentage-corrector/).