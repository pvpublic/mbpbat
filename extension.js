const Panel = imports.ui.main.panel;
const { GObject, Gio, UPowerGlib: UPower } = imports.gi;
const GLib = imports.gi.GLib;
const BaseIndicator = imports.ui.status.power.Indicator;

let aggregateMenu = Panel.statusArea['aggregateMenu'];
let origIndicator;
let newIndicator;

const BatPath = "/sys/class/power_supply/BAT0"

function init() {}

function enable() {
	origIndicator = aggregateMenu._power;
	newIndicator = new Indicator();
	aggregateMenu._indicators.replace_child(origIndicator, newIndicator);
}

function disable() {
	aggregateMenu._indicators.replace_child(newIndicator, origIndicator);
}

function readFile(filepath) {
	return String(GLib.file_get_contents(filepath)[1]).replace("\n", "");
}

var Indicator = GObject.registerClass(
	class Indicator extends BaseIndicator {
		_getCorrectPercentage() {
			let charge_full = parseInt(readFile(BatPath + "/charge_full"));
			let charge_now = parseInt(readFile(BatPath + "/charge_now"));
			let percentage = Math.trunc((charge_now/charge_full) * 100);
			percentage = Math.min(percentage, 100);
			return percentage;
		}
		_sync() {
			super._sync();
			let percentage = this._getCorrectPercentage();
			this._percentageLabel.text = percentage + "%";

			//------------- All below is modified copy paste from power.js ---------------
			// The icons
        	let chargingState = this._proxy.State === UPower.DeviceState.CHARGING
            	? '-charging' : '';
        	let fillLevel = 10 * Math.floor(percentage / 10);
        	const charged =
            	this._proxy.State === UPower.DeviceState.FULLY_CHARGED ||
            	(this._proxy.State === UPower.DeviceState.CHARGING && fillLevel === 100);
        	const icon = charged
            	? 'battery-level-100-charged-symbolic'
            	: 'battery-level-%d%s-symbolic'.format(fillLevel, chargingState);

        	// Make sure we fall back to fallback-icon-name and not GThemedIcon's
        	// default fallbacks
        	let gicon = new Gio.ThemedIcon({
            	name: icon,
            	use_default_fallbacks: false,
        	});

        	this._indicator.gicon = gicon;
        	this._item.icon.gicon = gicon;

        	// Not sure if neccesary to do after it has already been done in super.sync()
        	let fallbackIcon = this._proxy.IconName;
        	this._indicator.fallback_icon_name = fallbackIcon;
        	this._item.icon.fallback_icon_name = fallbackIcon;

        	// The icon label
        	const label = _('%d\u2009%%').format(percentage);
        	this._percentageLabel.text = label;
		}
	}
);
